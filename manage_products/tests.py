from django.test import TestCase, Client
from manage_products.fixtures import ProductFactory1, ProductFactory2
from manage_products.models import Product
from django.core.exceptions import ObjectDoesNotExist

from decimal import Decimal
import json


class TestModels(TestCase):
    def setUp(self):
        self.product1 = ProductFactory1()
        self.product2 = ProductFactory2()
        self.product3 = Product.objects.create(name="Lays Potato Chips", company="Frito", price=3.99)

    def test_products_exist(self):
        self.assertEqual(self.product1.name, "Chewbacca Action Figure")
        self.assertIsInstance(self.product1, Product)
        self.assertIsInstance(self.product2, Product)
        self.assertIsInstance(self.product3, Product)


class TestAPI(TestCase):
    def setUp(self):
        self.client = Client()
        self.product1 = ProductFactory1()
        self.product2 = ProductFactory2()
        self.product3 = Product.objects.create(name="Lays Potato Chips", company="Frito", price=3.99)        
        self.new_product = json.dumps({'name':"iphone 9", 'company':'apple', 'price':92.17})
        self.update = json.dumps({'name':"Chewie", 'price':29.99})
        self.search1 = json.dumps({'name':"Chewbacca Action Figure"})
        self.search2 = json.dumps({'company':"Hasbro"})
        self.search3 = json.dumps({'price':19.99})
        self.search4 = json.dumps({'name':"Honda Civic"})

    def test_root_requests(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content.decode('utf-8'))['Products'][0]['name'], "Chewbacca Action Figure")
        self.assertEqual(json.loads(resp.content.decode('utf-8'))['Products'][1]['name'], "Han Solo Action Figure")

    def test_get_single_product(self):
        resp = self.client.get('/1')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content.decode('utf-8'))['product']['company'], "Hasbro")

    def test_create_new_product(self):
        resp = self.client.post('/new', content_type='application/json', data=self.new_product)
        self.assertEqual(resp.status_code, 200)
        self.assertJSONEqual(resp.content.decode('utf-8'), {'product': {'company': 'apple', 'name': 'iphone 9', 'price': 92.17}})

    def test_delete_product(self):
        self.assertEqual(Product.objects.get(pk=1).name, 'Chewbacca Action Figure')
        resp = self.client.delete('/1')
        self.assertEqual(resp.status_code, 200)
        with self.assertRaises(ObjectDoesNotExist):
            Product.objects.get(pk=1)

    def test_update_product(self):
        self.assertEqual(Product.objects.get(pk=1).name, 'Chewbacca Action Figure')
        resp = self.client.put('/1', content_type='application/json', data=self.update)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(Product.objects.get(pk=1).name, 'Chewie')
        self.assertEqual(Product.objects.get(pk=1).price, Decimal('29.99'))

    def test_search(self):
        resp = self.client.post('/search', content_type='application/json', data=self.search1)
        results1 = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(len(results1["Products"]), 1)
        resp = self.client.post('/search', content_type='application/json', data=self.search2)
        results2 = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(len(results2["Products"]), 2)
        resp = self.client.post('/search', content_type='application/json', data=self.search3)
        results3 = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(len(results3["Products"]), 2)
        resp = self.client.post('/search', content_type='application/json', data=self.search4)
        results4 = json.loads(resp.content.decode('utf-8'))
        self.assertEqual(results4["Products"], [])