import factory

from manage_products.models import Product


class ProductFactory1(factory.DjangoModelFactory):
    class Meta:
        model = Product

    name = "Chewbacca Action Figure"
    company = "Hasbro"
    price = 19.99


class ProductFactory2(factory.DjangoModelFactory):
    class Meta:
        model = Product

    name = "Han Solo Action Figure"
    company = "Hasbro"
    price = 19.99