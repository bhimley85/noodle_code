from django.conf.urls import patterns, include, url
from manage_products import views


urlpatterns = patterns('',
    url(r'^$', views.AllProducts.as_view(), name="all_products"),
    url(r'^(?P<product_id>[\d]+)', views.SingleProduct.as_view(), name="single_product"),
    url(r'^new', views.CreateNewProduct.as_view(), name="create_new_product"),
    url(r'^search', views.SearchProducts.as_view(), name="search_products"),
    url(r'^attribute', views.Attribute.as_view(), name="attribute" )
)
