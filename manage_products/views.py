from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import View
from manage_products.models import Product
from django.core.exceptions import ObjectDoesNotExist

from decimal import Decimal
import json


class SingleProduct(View):
    def get(self, request, product_id):
        if not Product.objects.filter(pk=product_id).exists():
            return JsonResponse({'error':'product with id={} not found'.format(product_id)})
        product = Product.objects.get(pk=product_id)
        return JsonResponse({'product': {'name':product.name, 'company':product.company, 'price':product.price}})

    def put(self, request, product_id):
        if not Product.objects.filter(pk=product_id).exists():
            return JsonResponse({'error':'product with id={} not found'.format(product_id)})
        update_info = json.loads(request.body.decode('utf-8'))
        product = Product(pk=product_id, **update_info)
        product.save()
        return JsonResponse({'product': {'name':product.name, 'company':product.company, 'price':product.price}})

    def delete(self, request, product_id):
        if not Product.objects.filter(pk=product_id).exists():
            return JsonResponse({'error':'product with id={} not found'.format(product_id)})
        product = Product.objects.get(pk=product_id)
        product.delete()
        return JsonResponse({'success':"product id#{} was deleted".format(product_id)})


class CreateNewProduct(View):
    def post(self, request):
        new_product = json.loads(request.body.decode('utf-8'))
        try:
            product = Product.objects.create(name=new_product['name'], 
                                            company=new_product['company'], 
                                            price=new_product['price'])
            return JsonResponse({'product': {'name':product.name, 'company':product.company, 'price':product.price}})
        except Exception as e:
            return JsonResponse({'error':e})


class Attribute(View):
    # Add Attribute
    def post(self, request):
        pass
        
    # Remove Attribute
    def delete(self, request):
        pass               


class AllProducts(View):
    def get(self, request):
        products = [{'name':p.name, 'company':p.company, 'price':p.price} for p in Product.objects.all()]
        return JsonResponse({'Products':products})


class SearchProducts(View):
    def post(self, request):
        search_info = json.loads(request.body.decode('utf-8'))
        products = [{'name':p.name, 'company':p.company, 'price':p.price} for p in Product.objects.filter(**search_info)]
        return JsonResponse({"Products":products})
